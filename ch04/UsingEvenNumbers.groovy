def sum(n) {
	total = 0
	for (int i = 2; i <= n; i += 2)
		total += i
	total
}

def product(n) {
	total = 1
	for (int i = 2; i <= n; i += 2)
		total *= i
	total
}

def sqr(n) {
	squared = []
	for (int i = 2; i <= n; i += 2)
		squared << i ** 2
	squared
}

println "Sum of even numbers from 1 to 10: ${sum(10)}"
println "Product of even numbers from 1 to 10: ${product(10)}"
println "Squares of even numbers from 1 to 10: ${sqr(10)}"