def pickEven(n, block) {
	for (int i = 2; i <= n; i+= 2)
		block(i)
}

//pickEven(10, { println it })
//pickEven(10) { println it }
//pickEven(10) { it -> println it }

total = 0
prod = 1
sqrs = []

pickEven(10) { total += it }
pickEven(10) { prod *= it }
pickEven(10) { sqrs << it ** 2 }

println "Sum of even numbers from 1 to 10: ${total}"
println "Product of even numbers from 1 to 10: ${prod}"
println "Squares of even numbers from 1 to 10: ${sqrs}"