def splitName(name) { name.split('\\s')}

def (v1, v2, v3, v4) = splitName("James Bond Agent 007")

println "$v3 $v4, $v2, $v1 $v2"

def (t1, t2) = "Charles Darwin".split(' ')
println "$t1, $t2"

def n1 = "Charles"
def n2 = "Darwin"

println "$n1, $n2"
(n1, n2) = [n2, n1]
println "$n1, $n2"

def (String cat, mouse) = ["TOm", "Jerry", "Spyke", "Tyke"]
println "$cat, $mouse"

def (String cat2, mouse2, Integer dog) = ["TOm", "Jerry"]
// It throws an exception because int variable dog can not be set to null
// def (String cat2, mouse2, int dog) = ["TOm", "Jerry"]
println "$cat2, $mouse2, $dog"