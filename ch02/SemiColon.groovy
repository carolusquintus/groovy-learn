class Semi {
	static {
		println "Class Initializer called..."
	}

	{
		println "Instance Initializer called..."
	}

	def val = 3
}

println new Semi()