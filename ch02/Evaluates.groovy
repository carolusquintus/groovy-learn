String obj = "hello"
int val = 4

if (obj) println "exists $obj"
if (val) println "exists $val"

lst0 = null
println lst0 ? "lst0 true" : "lst0 false"

lst0 = []
println lst0 ? "lst0 true" : "lst0 false"

lst0 = [1]
println lst0 ? "lst0 true" : "lst0 false"