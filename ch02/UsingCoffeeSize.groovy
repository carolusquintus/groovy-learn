enum CoffeSize { SHORT, SMALL, MEDIUM, LARGE, MUG }

def orderCoffe(size) {
	print "Coffee order received for size $size: "
	switch (size) {
		case [CoffeSize.SHORT, CoffeSize.SMALL]:
			println "you're health conscious"
			break
		case CoffeSize.MEDIUM..CoffeSize.LARGE:
			println "you gotta be a programmer"
			break
		case CoffeSize.MUG:
			println "you should try Caffeine IV"
			break
	}
}

orderCoffe CoffeSize.SMALL
orderCoffe CoffeSize.LARGE
orderCoffe CoffeSize.MUG

print "Available size are: "

for (size in CoffeSize.values()) {
	print "$size "
}