def receiveVarArgs(a, int... b) {
	println "You passed $a and $b"
}

def receiveArray(a, int [] b) {
	println "You passed $a and $b"
}

val = [1,2,3,4,5,6,7,8,9,10]

receiveVarArgs 20,21,22,23,24,25,26,27
receiveArray 20,21,22,23,24,25,26,27

receiveVarArgs 1, val as int[]
receiveArray 1, val