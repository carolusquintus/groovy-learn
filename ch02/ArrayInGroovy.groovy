//int[] arr = int[] {1,2,3,4}; // Not in Groovy
int[] arr = [1,2,3,4] // Not in Groovy

println arr
println "class is " + arr.class.name

def arr2 = [1,2,3,4] as int[]

println arr2
println "class is " + arr2.class.name