import java.util.*;

public class UsingCollection {
	public static void main(String[] args) {
		ArrayList<String> alst = new ArrayList<String>();
		Collection<String> col = alst;
		List<String> lst = alst;

		alst.add("one");
		alst.add("two");
		alst.add("three");

		alst.remove(0);
		col.remove(0);
		lst.remove(0);

		System.out.println("Added three times, removed 3, so no items to remain.");
		System.out.println("Number of elements is: " + alst.size());
		System.out.println("Number of elements is: " + col.size());
		System.out.println("Number of elements is: " + lst.size());
	}
}