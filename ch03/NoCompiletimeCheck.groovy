import groovy.transform.*

@TypeChecked
def shout(String str) {
	println "Printing in uppercase"
	println str.toUpperCase()
	println "Printin again in uppercase"
	println str.toUpperCase()	
}

try {
	shout('hello')
} catch(ex) {
	println "Failed..."
	println ex
}