import groovy.transform.*

@TypeChecked
class Sample {
	def method1() {}

	@TypeChecked(TypeCheckingMode.SKIP)
	def method2(String str) {
		str.shout()
	}
}

def str = "hello"
str.metaClass.shout = { -> toUpperCase() }

def sam = new Sample()
sam.method2(str)