public class Car implements Cloneable {
	Engine engine = new Engine();

	public Object clone() {
		try {
			Car cloned = (Car) super.clone();
			cloned.engine = (Engine) engine.clone();

			return cloned;
		} catch(CloneNotSupportedException e) {
			return null;
		}
	}

	public static void main(String[] args) {
		Car car1 = new Car();
		Car car2 = (Car) car1.clone();

		System.out.println(car1);
		System.out.println(car1.engine);
		System.out.println(car2);
		System.out.println(car2.engine);
	}
}

class Engine implements Cloneable {
	public Object clone() {
		try {
			Engine engine = (Engine) super.clone();

			return engine;
		} catch(CloneNotSupportedException e) {
			return null;
		}
	}
}